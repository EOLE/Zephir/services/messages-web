####
#### Temporary layer to prepare installation
####
FROM ubuntu:bionic AS build

# Packages required to build
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    curl \
    gettext \
    git \
    locales \
    make \
    python3 \
    python3-ipy \
    python3-lxml \
    python3-setuptools \
    python3-yaml

ARG CONTAINERPILOT_VERSION=3.4.3
ARG CONTAINERPILOT_CHECKSUM=e8258ed166bcb3de3e06638936dcc2cae32c7c58

RUN curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CONTAINERPILOT_CHECKSUM}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /tmp

ARG CONSULTEMPLATE_VERSION=0.19.3
ARG CONSULTEMPLATE_CHECKSUM=6467b442d8fd68ca0a1eba35f42edb6f3b9941e10b6d58688edc3506fca0bb19

RUN curl -Lso /tmp/consul-template.tar.gz \
         "https://releases.hashicorp.com/consul-template/${CONSULTEMPLATE_VERSION}/consul-template_${CONSULTEMPLATE_VERSION}_linux_amd64.tgz" \
    && echo "${CONSULTEMPLATE_CHECKSUM}  /tmp/consul-template.tar.gz" | sha256sum -c \
    && tar zxf /tmp/consul-template.tar.gz -C /tmp


# Orcherstration using containerpilot
# Configuration using consul-template
ARG CONTAINERPILOT_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/Services/orchestrate-containerpilot.git
ARG CONTAINERPILOT_REPO_REF=223dafd3d093702c8717292247e65d0c55ffb513

RUN git clone "${CONTAINERPILOT_REPO_URL}" "/tmp/orchestrate" \
    && cd /tmp/orchestrate \
    && git checkout "${CONTAINERPILOT_REPO_REF}"


# API messages description
ARG MESSAGES_API_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/messages-api.git
ARG MESSAGES_API_REPO_REF=0.1.0

RUN git clone "${MESSAGES_API_REPO_URL}" "/tmp/messages-api" \
    && cd /tmp/messages-api \
    && git checkout "${MESSAGES_API_REPO_REF}"


# Common python Zéphir library
ARG PYTHON_ZEPHIR_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/python-zephir.git
ARG PYTHON_ZEPHIR_REPO_REF=0.1.0

RUN git clone "${PYTHON_ZEPHIR_REPO_URL}" "/tmp/python-zephir" \
    && cd /tmp/python-zephir \
    && git checkout "${PYTHON_ZEPHIR_REPO_REF}"


##
## Service specific sources
##

# Tiramisu
ARG TIRAMISU_REPO_URL=https://framagit.org/tiramisu/tiramisu.git
ARG TIRAMISU_REPO_REF=fee8997f1371d2ae18b700950c8aa3cc2e4e6a8c
ARG TIRAMISU_DEST_DIR=/tmp/build/tiramisu
# XXX: specific stable tag in the feature/3.0 tiramisu development branch
RUN git clone "${TIRAMISU_REPO_URL}" /tmp/tiramisu \
    && cd /tmp/tiramisu \
    && git checkout "${TIRAMISU_REPO_REF}" \
    && make DESTDIR=${TIRAMISU_DEST_DIR} build-lang install-lang \
    && python3 setup.py install --root ${TIRAMISU_DEST_DIR}


# Tiramisu Web
ARG TIRAMISU_WEB_REPO_URL=https://forge.cadoles.com/egarette/tiramisu-web.git
ARG TIRAMISU_WEB_REPO_REF=9ce55c7d3262c0013f9c3a6e509bd15518c39724
ARG TIRAMISU_WEB_DEST_DIR=/tmp/build/tiramisu-web
RUN git clone "${TIRAMISU_WEB_REPO_URL}" /tmp/tiramisu-web \
    && cd /tmp/tiramisu-web \
    && git checkout "${TIRAMISU_WEB_REPO_REF}"


####
#### Target layer
####
FROM ubuntu:bionic

MAINTAINER Pôle EOLE <eole@ac-dijon.fr>

# Packages required for working service
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    gnupg \
    jq \
    locales \
    openssl \
    python3 \
    python3-aiohttp \
    python3-autobahn \
    python3-dev \
    python3-jwt \
    python3-psycopg2 \
    python3-requests \
    python3-yaml \
    sqitch \
    tzdata

# For debug only
RUN apt-get update -y && apt-get install -y \
    curl \
    iproute2 \
    iputils-ping \
    vim

# Force french locale
RUN locale-gen fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LC_ALL fr_FR.UTF-8

# Force timezone
RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Sqitch
RUN sqitch config --user user.name 'Equipe EOLE'\
    && sqitch config --user user.email 'eole@ac-dijon.fr'

# Install tools from build layer
COPY --from=build /tmp/containerpilot /usr/local/bin
COPY --from=build /tmp/consul-template /usr/local/bin

# consul-template wrapper to generate /etc/zephir-services.conf
COPY --from=build /tmp/orchestrate/update-service-conf /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Service template for consul-template
ENV services_conf_filename=zephir-services.conf
ARG services_conf=/etc/zephir-services.conf
ENV services_conf=$services_conf
COPY --from=build /tmp/orchestrate/${services_conf_filename}.ctmpl ${services_conf}.ctmpl

# Install libraries required by service
COPY --from=build /tmp/python-zephir/zephir /usr/lib/python3/dist-packages/zephir
COPY --from=build /tmp/messages-api/messages /srv/messages

# Manage container with ContainerPilot
CMD ["/usr/local/bin/containerpilot", "-config", "/etc/containerpilot.json5"]
COPY --from=build /tmp/orchestrate/containerpilot.json5 /etc/containerpilot.json5


##
## Service specific
##

RUN apt-get update -y && apt-get install -y \
    python3 \
    python3-ipy \
    python3-lxml

# Tiramisu
COPY --from=build /tmp/build/tiramisu/usr/share/locale /usr/share/locale
COPY --from=build /tmp/build/tiramisu/usr/local/lib/python3.6/dist-packages/tiramisu/ /usr/local/lib/python3.6/dist-packages/tiramisu/
COPY --from=build /tmp/tiramisu/tiramisu/storage/cache /usr/local/lib/python3.6/dist-packages/tiramisu/storage/cache
COPY --from=build /tmp/tiramisu/tiramisu/storage/dictionary /usr/local/lib/python3.6/dist-packages/tiramisu/storage/dictionary
COPY --from=build /tmp/tiramisu/tiramisu/storage/sqlalchemy /usr/local/lib/python3.6/dist-packages/tiramisu/storage/sqlalchemy
COPY --from=build /tmp/tiramisu/tiramisu/storage/sqlite3 /usr/local/lib/python3.6/dist-packages/tiramisu/storage/sqlite3

# Tiramisu Web
COPY --from=build /tmp/tiramisu-web/src/python/tiramisu_web /usr/lib/python3/dist-packages/tiramisu_web

# Library
COPY src/python /usr/lib/python3/dist-packages/messages_web

# Controller
RUN mkdir -p /srv/bin /srv/zephir /srv/html
COPY src/javascript /srv/html/javascript
COPY src/bower_components /srv/html/bower_components
COPY scripts/* /srv/bin
RUN chmod +x /srv/bin/*
