from zephir.message import list_messages, parse_definition, get_message
from collections import OrderedDict
from os.path import join, basename, dirname
from glob import glob

from tiramisu import StrOption, IntOption, BoolOption, ChoiceOption, OptionDescription, Config
ALLOW_PRIVATE = False
ROOTPATH = join('..')


def _get_description(description,
                     name):
    # hack because some description are, in fact some help
    if not '\n' in description and len(description) <= 150:
        doc = description
    else:
        doc = name
    return doc


def _get_option(name,
                arg,
                file_path):
    """generate option
    """
    props = []
    if not hasattr(arg, 'default'):
        props.append('mandatory')
    description = arg.description.strip().rstrip()
    kwargs = {'name': name,
              'doc': _get_description(description, name),
              'properties': frozenset(props)}
    #if arg.multiple:
    #    kwargs['multi'] = True
    if hasattr(arg, 'default'):
        kwargs['default'] = arg.default
    type_ = arg.type
    if type_ == 'Dict' or 'String' in type_:
        return StrOption(**kwargs)
    elif 'Number' in type_ or type_ == 'ID' or type_ == 'Integer':
        return IntOption(**kwargs)
    elif type_ == 'Boolean':
        return BoolOption(**kwargs)
    raise Exception('unsupported type {} in {}'.format(type_, file_path))


def _parse_args(message_def,
                options,
                file_path,
                needs):
    """build option with args/kwargs
    """
    new_options = OrderedDict()
    #for idx, arg in enumerate(message_def.args):
    #    name = 'tiramisu_' + str(idx)
    #    if not arg.required:
    #        raise Exception('arg without required ?? in {}'.format(file_path))
    #    new_options[name] = arg
    #    if arg.needs:
    #        needs.setdefault(message_def.uri, {}).setdefault(arg.needs, []).append(name)
    for name, arg in message_def.parameters.items():
        #if name.startswith('tiramisu_'):
        #    raise Exception('multi args with name {} in {}'.format(name, file_path))
        new_options[name] = arg
        if arg.ref:
            needs.setdefault(message_def.uri, {}).setdefault(arg.ref, []).append(name)
    for name, arg in new_options.items():
        options.append(_get_option(name, arg, file_path))


def _parse_responses(message_def,
                     file_path):
    """build option with returns
    """
    responses = OrderedDict()
    if message_def.response:
        keys = {'': {'description': '',
                     'columns': {}}}
        provides = {}
        to_list = True
        param_type = message_def.response.type

        if param_type.startswith('[]'):
            to_list = False
            param_type = param_type[2:]
        if param_type in ['Dict', 'File']:
            pass
            #break
        if message_def.response.parameters is not None:
            for name, obj in message_def.response.parameters.items():
                if name in responses:
                    raise Exception('multi response with name {} in {}'.format(name, file_path))
                #param_type = obj.type
                descr = obj.description.strip().rstrip()
                #if message_def.parameters:
                #    for custom_name, custom_param in message_def.parameters.items():
                #        param_type2 = custom_param.type
                #        #if param_type2 in message_def.customtype:
                #        #    keys[custom_name] = {'description': custom_descr,
                #        #                         'columns': {}}
                #        #    customtype2 = message_def.customtype[param_type2]
                #        #    for custom_name2, custom_param2 in customtype2['parameters'].items():
                #        #        keys[custom_name]['columns'][custom_name2] = {'description': custom_param2['description'].strip().rstrip(),
                #        #                                                      'type': param_type2}
                #        #        ref = custom_param2.get('ref')
                #        #        if ref:
                #        #            provides[custom_name2] = ref
                #        #else:
                #        keys['']['columns'][custom_name] = {'description': custom_param.description,
                #                                            'type': param_type2}
                #        ref = custom_param.ref
                #        if ref:
                #            provides[custom_name] = ref
                #else:
                keys['']['columns'][name] = {'description': obj.description,
                                                    'type': obj.type}
                ref = obj.ref
                if ref:
                    provides[name] = ref
                #if param_type in message_def.customtype:
                #    customtype = message_def.customtype[param_type]
                #    key = name
                #    if customtype['type'] == 'Dict':
                #        for custom_name, custom_param in customtype['parameters'].items():
                #            param_type2 = custom_param['type']
                #            #if param_type2.startswith('[]'):
                #            #    break
                #            #if param_type2 in ['Dict', 'File']:
                #            #    break
                #            custom_descr = custom_param['description'].strip().rstrip()
                #            if param_type2 in message_def.customtype:
                #                keys[custom_name] = {'description': custom_descr,
                #                                     'columns': {}}
                #                customtype2 = message_def.customtype[param_type2]
                #                for custom_name2, custom_param2 in customtype2['parameters'].items():
                #                    keys[custom_name]['columns'][custom_name2] = {'description': custom_param2['description'].strip().rstrip(),
                #                                                                  'type': param_type2}
                #                    ref = custom_param2.get('ref')
                #                    if ref:
                #                        provides[custom_name2] = ref
                #            else:
                #                keys['']['columns'][custom_name] = {'description': custom_descr,
                #                                                    'type': param_type}
                #                ref = custom_param.get('ref')
                #                if ref:
                #                    provides[custom_name] = ref
                #        else:
                #            continue
                #        break
                #    break
            else:
                keys['']['columns'][name] = {'description': descr,
                                             'type': obj.type}
                ref = obj.ref
                if ref:
                    provides[name] = ref
                responses['keys'] = keys
                responses['to_list'] = to_list
                responses['to_dict'] = False
                responses['provides'] = provides
        else:
            name = 'response'
            keys['']['columns'][name] = {'description': message_def.response.description,
                                         'type': message_def.response.type}
            ref = message_def.response.ref
            if ref:
                provides[name] = ref
            responses['keys'] = keys
            responses['to_list'] = to_list
            responses['to_dict'] = True
            responses['provides'] = provides
    return responses


def _getoptions_from_yml(message_def,
                         version,
                         optiondescriptions,
                         file_path,
                         needs):
    if message_def.pattern == 'event' and message_def.response:
        raise Exception('event with response?: {}'.format(file_path))
    if message_def.pattern == 'rpc' and not message_def.response:
        print('rpc without response?: {}'.format(file_path))
    options = [StrOption('version',
                         'version',
                         version,
                         properties=frozenset(['hidden']))]
    _parse_args(message_def, options, file_path, needs)
    name = message_def.uri
    description = message_def.description.strip().rstrip()
    optiondescriptions[name] = (description, options)


def _get_root_option(optiondescriptions):
    """get root option
    """
    domains = OrderedDict()
    for name in optiondescriptions.keys():
        domains.setdefault(name.split('.')[0], []).append(name)

    domains_list = list(domains.keys())
    domains_list.sort()
    domain_option = ChoiceOption('domain',
                                 'Sélectionner le domaine',
                                 tuple(domains_list),
                                 properties=frozenset(['mandatory']))
    options_obj = [domain_option]
    messages = OrderedDict()
    for domain in domains_list:
        requires = [{'option': domain_option,
                     'expected': domain,
                     'action': 'disabled',
                     'inverse': True}]
        option = ChoiceOption('message_' + domain,
                              'Sélectionner le message du domaine {}'.format(domain),
                              tuple(domains[domain]),
                              requires=requires,
                              properties=frozenset(['mandatory']))
        options_obj.append(option)
        messages[domain] = option
    for name, options_descr in optiondescriptions.items():
        description, options = options_descr
        message = messages[name.split('.')[0]]
        if len(options) != 1:
            requires = [{'option': message,
                         'expected': name,
                         'action': 'disabled',
                         'inverse': True}]
            properties = None
        else:
            requires = None
            properties = frozenset(['disabled'])
        options_obj.append(OptionDescription(name.replace('.', '_'),
                                             description,
                                             options,
                                             properties=properties,
                                             requires=requires))
    return OptionDescription('root', 'root', options_obj)


def get_messages():
    """generate description from yml files
    """
    optiondescriptions = OrderedDict()
    responses = OrderedDict()
    needs = OrderedDict()
    messages = list(list_messages())
    messages.sort()
    for message_name in messages:
        message_def = get_message(message_name)
        version = message_name.split('.')[0]
        if message_def.pattern not in ['rpc', 'event'] or \
                (not message_def.public and not ALLOW_PRIVATE):
            continue
        if message_def.uri in responses:
            raise Exception('uri {} allready loader'.format(message_def.uri))
        _getoptions_from_yml(message_def,
                             version,
                             optiondescriptions,
                             message_name,
                             needs)
        responses[message_def.uri] = _parse_responses(message_def,
                                                      message_name)

    root = _get_root_option(optiondescriptions)
    try:
        api = Config(root)
    except Exception as err:
        raise Exception('error when generating root optiondescription: {}'.format(err))

    return needs, responses, api
